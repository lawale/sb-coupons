<?php

use App\Models\Event;
use App\Models\PromoCode;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Symfony\Component\HttpFoundation\Response;

class PromoCodeTest extends TestCase
{
    use DatabaseMigrations;

    public function testRetrieveAllPromoCodes()
    {
        $promoCodes = factory(PromoCode::class, 10)->create();
        $this->delete("/api/v1/promo_codes/" . $promoCodes[rand(0, 9)]->id);

        $response = $this->get('/api/v1/promo_codes');

        $response->assertResponseStatus(Response::HTTP_OK);
        $response->seeJsonStructure([
            'status', 'meta',
            'data' => [
                '*' => ['id', 'code', 'radius', 'worth', 'created_at', 'expires_at', 'status']
            ]
        ]);
    }

    public function testRetrieveActivePromoCodes()
    {
        $promoCodes = factory(PromoCode::class, 10)->create();

        $this->delete("/api/v1/promo_codes/" . $promoCodes[rand(0, 9)]->id);
        $this->delete("/api/v1/promo_codes/" . $promoCodes[rand(0, 9)]->id);

        $response = $this->get('/api/v1/promo_codes?is_active=1');

        $response->assertResponseStatus(Response::HTTP_OK);
        $response->seeJsonStructure([
            'status', 'meta',
            'data' => [
                '*' => ['id', 'code', 'radius', 'worth', 'created_at', 'expires_at']
            ],
        ]);
    }

    public function testRetrieveNoPromoCodes()
    {
        $response = $this->get('/api/v1/promo_codes');

        $response->assertResponseStatus(Response::HTTP_OK);
        $response->seeJsonStructure(['status', 'data' => []]);
    }

    public function testCreationSuccess()
    {
        $event = factory(Event::class)->create();

        $data = [
            'event_id' => $event->id,
            'currency' => 'NGN',
            'radius' => 50,
            'worth' => 500.50,
            'expires_at' => date('Y-m-d', strtotime("+3 days"))
        ];
        $response = $this->post("/api/v1/promo_codes/", $data);

        $response->assertResponseStatus(Response::HTTP_CREATED);
        $response->seeJsonStructure([
            'status',
            'data' => ['id', 'code', 'radius', 'worth', 'created_at', 'expires_at'],
        ]);
    }

    public function testCreationValidationError()
    {
        $response = $this->post("/api/v1/promo_codes/", []);

        $response->assertResponseStatus(Response::HTTP_BAD_REQUEST);
        $response->seeJsonStructure(['status', 'message', 'data']);
    }

    public function testRadiusConfigurationSuccess()
    {
        $promoCode = factory(PromoCode::class)->create();

        $response = $this->patch("/api/v1/promo_codes/{$promoCode->id}/radius", ['radius' => 500]);

        $response->assertResponseStatus(Response::HTTP_OK);
        $response->seeJsonStructure([
            'status',
            'data' => ['id', 'code', 'radius', 'worth', 'created_at', 'expires_at'],
        ]);
    }

    public function testRadiusConfigurationError()
    {
        $promoCode = factory(PromoCode::class)->create();

        $response = $this->patch("/api/v1/promo_codes/{$promoCode->id}/radius", []);

        $response->assertResponseStatus(Response::HTTP_BAD_REQUEST);
        $response->seeJsonStructure(['status', 'message', 'data']);
    }

    public function testRadiusConfigurationPromoCodeNotFound()
    {
        $response = $this->patch("/api/v1/promo_codes/0/radius", []);

        $response->assertResponseStatus(Response::HTTP_NOT_FOUND);
        $response->seeJsonStructure(['status', 'message', 'data']);
    }

    public function testPromoCodeValiditySuccess()
    {
        factory(Event::class)->create();
        $promoCode = factory(PromoCode::class)->create();

        $payload = [
            'code' => $promoCode->code,
            'origin' => ['latitude' => 7.7634697, 'longitude' => 4.5341617],
            'destination' => ['latitude' => 7.760891, 'longitude' => 4.5329985],
        ];
        $response = $this->post('/api/v1/promo_codes/validate', $payload);

        $response->seeJsonStructure([
            'status',
            'data' => ['id', 'code', 'radius', 'worth', 'created_at', 'expires_at', 'polyline'],
        ]);
        $response->assertResponseStatus(Response::HTTP_OK);
    }

    public function testPromoCodeValidityFailure()
    {
        factory(Event::class)->create();
        $promoCode = factory(PromoCode::class)->create();

        $payload = [
            'code' => $promoCode->code,
            'origin' => ['latitude' => 40.7634697, 'longitude' => 20.5341617],
            'destination' => ['latitude' => 30.7650869, 'longitude' => 80.5375614],
        ];

        $response = $this->post('/api/v1/promo_codes/validate', $payload);
        $response->assertResponseStatus(Response::HTTP_BAD_REQUEST);
        $response->seeJsonStructure(['status', 'message']);
    }

    public function testPromoCodeValidityValidationError()
    {
        $response = $this->post('/api/v1/promo_codes/validate', []);

        $response->assertResponseStatus(Response::HTTP_BAD_REQUEST);
        $response->seeJsonStructure(['status' => [], 'message']);
    }

    public function testPromoCodeValidityPromoCodeNotFound()
    {
        $payload = [
            'code' => 'fish',
            'origin' => ['latitude' => 40.7634697, 'longitude' => 20.5341617],
            'destination' => ['latitude' => 30.7650869, 'longitude' => 80.5375614],
        ];

        $response = $this->post('/api/v1/promo_codes/validate', $payload);

        $response->assertResponseStatus(Response::HTTP_BAD_REQUEST);
        $response->seeJsonStructure(['status']);
    }

    /**
     * Test promo code deactivation endpoint
     *
     * @return void
     */
    public function testDeactivationSuccess()
    {
        $promoCode = factory(PromoCode::class)->create();

        $this->delete("/api/v1/promo_codes/" . $promoCode->id);
        $this->assertResponseStatus(Response::HTTP_NO_CONTENT);
    }

    /**
     * Test promo code deactivation endpoint for non-existent promo code
     *
     * @return void
     */
    public function testPromoCodeNotFound()
    {
        $response = $this->delete("/api/v1/promo_codes/0");
        $response->seeJsonStructure(['status', 'message']);
        $response->assertResponseStatus(Response::HTTP_NOT_FOUND);
    }
}
