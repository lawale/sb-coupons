<?php

use App\Models\Event;
use Laravel\Lumen\Testing\DatabaseMigrations;

class EventTest extends TestCase
{
    use DatabaseMigrations;

    public function testIndex()
    {
        factory(Event::class, 10)->create();
        $this->get('/api/v1/events');

        $this->seeStatusCode(200);
        $this->seeJsonStructure(['data', 'status', 'meta']);
    }

    public function testCreateEventSuccess()
    {
        $data = [
            'name' => 'Random Event',
            'longitude' => 89.5,
            'latitude' => 90,
            'starts_at' => '2020-01-20 00:30',
            'ends_at' => '2021-01-20 09:00'
        ];

        $this->post('/api/v1/events', $data);

        $this->seeStatusCode(201);
        $this->seeJsonStructure([
            'status',
            'data' => ['id', 'name', 'longitude', 'latitude', 'starts_at', 'ends_at', 'created_at', 'updated_at']
        ]);
    }

    public function testCreateEventFailure()
    {
        $response = $this->post('/api/v1/events', []);
        $response->seeJsonStructure(['status', 'message', 'data',]);

        $response->assertResponseStatus(400);
    }
}
