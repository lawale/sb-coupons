<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use League\Fractal\Manager;
use League\Fractal\Serializer\JsonApiSerializer;

class FractalManagerProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Manager::class, function ($app) {
            $manager = new Manager();
            $base = app(Request::class)->getBaseURL();
            $manager->setSerializer(new JsonApiSerializer($base));
            return $manager;
        });
    }
}
