<?php

namespace App\Http\Controllers;

use App\Constants\ResponseCodes;
use App\Models\Event;
use App\Transformers\EventTransformer;
use App\Validations\EventCreationValidation;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Symfony\Component\HttpFoundation\Response;

class EventsController extends Controller
{
    /**
     * @param Manager $fractal
     * @return JsonResponse
     */
    public function index(Manager $fractal): JsonResponse
    {
        try {
            $events = Event::paginate();

            $resource = new Collection($events, new EventTransformer());
            $resource->setPaginator(new IlluminatePaginatorAdapter($events));
            $data = $fractal->createData($resource)->toArray();

            return $this->sendSuccess($data);
        } catch (Exception $ex) {
            return $this->sendError(
                ResponseCodes::UNEXPECTED_ERROR,
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $ex->getMessage()
            );
        }
    }

    /**
     * @param Manager $fractal
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Manager $fractal, Request $request): JsonResponse
    {
        $inputs = $request->all();

        $validator = new EventCreationValidation($inputs);
        if ($validator->validate()) {
            return $this->sendFailure($inputs, 400, $validator->getErrorMessage());
        }

        /** @var Event $event */
        $event = Event::create($inputs);
        $event->save();

        $resource = new Item($event, new EventTransformer());
        return $this->sendSuccess(
            $fractal->createData($resource)->toArray(),
            Response::HTTP_CREATED
        );
    }

    /**
     * @param Manager $fractal
     * @param $id
     * @return JsonResponse
     */
    public function show(Manager $fractal, $id): JsonResponse
    {
        $event = Event::find($id);
        if (!$event) {
            return $this->sendFailure([], Response::HTTP_NOT_FOUND, 'Record not found');
        }

        $resource = new Item($event, new EventTransformer());
        return $this->sendSuccess($fractal->createData($resource)->toArray());
    }
}
