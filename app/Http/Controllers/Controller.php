<?php

namespace App\Http\Controllers;

use App\Constants\ResponseMessages;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController
{
    public function sendSuccess($data, $statusCode = Response::HTTP_OK): JsonResponse
    {
        $response = array_merge(['status' => 'success'], $data);
        return $this->sendResponse($response, $statusCode);
    }

    public function sendFailure($data = null, $httpStatusCode = 400, $message = null): JsonResponse
    {
        $response = [
            'status' => 'fail',
            'message' => $message,
            'data' => $data
        ];

        return $this->sendResponse($response, $httpStatusCode);
    }

    public function sendError($code, $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR, $message = null): JsonResponse
    {
        $response = [
            'status' => 'error',
            'message' => $message ?? ResponseMessages::getMessageFromCode($code),
            'code' => $code
        ];

        return $this->sendResponse($response, $statusCode);
    }

    public function sendException()
    {

    }

    public function sendBadRequestResponse($message = null, $httpStatusCode = Response::HTTP_BAD_REQUEST): JsonResponse
    {
        return $this->sendFailure(
            [],
            $httpStatusCode,
            $message
        );
    }

    private function sendResponse($content, $statusCode): JsonResponse
    {
        return response()
            ->json($content, $statusCode)
            ->header('Content-Type', 'application/json');
    }
}
