<?php

namespace App\Http\Controllers;

use App\Constants\ResponseCodes;
use App\Models\PromoCode;
use App\Transformers\PromoCodeTransformer;
use App\Validations\PromoCodeCreationValidation;
use App\Validations\PromoCodeRadiusUpdateValidation;
use App\Validations\PromoCodeValidityValidation;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Polyline;
use Symfony\Component\HttpFoundation\Response;

class PromoCodesController extends Controller
{
    public function index(Manager $fractal, Request $request): JsonResponse
    {
        try {
            $promoCodes = PromoCode::query();
            $filter = (bool)$request->get('is_active');

            if ($filter) {
                $promoCodes = $promoCodes->where('status', '=', PromoCode::ACTIVE);
            }
            $promoCodes = $promoCodes->paginate(15);

            $resource = new Collection($promoCodes, new PromoCodeTransformer($filter));
            $resource->setPaginator(new IlluminatePaginatorAdapter($promoCodes));
            return $this->sendSuccess($fractal->createData($resource)->toArray());
        } catch (Exception $exception) {
            return $this->sendError(
                ResponseCodes::UNEXPECTED_ERROR,
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $exception->getMessage()
            );
        }
    }

    public function create(Manager $fractal, Request $request): JsonResponse
    {
        $inputs = $request->all();

        $validator = new PromoCodeCreationValidation($inputs);
        if (!$validator->validate()) {
            return $this->sendFailure($inputs, Response::HTTP_BAD_REQUEST, $validator->getErrors());
        }

        try {
            $promo = new PromoCode($inputs);
            $promo->code = $promo->generateCode();
            $promo->status = PromoCode::ACTIVE;
            $promo->save();

            $resource = new Item($promo, new PromoCodeTransformer());
            return $this->sendSuccess(
                $fractal->createData($resource)->toArray(),
                Response::HTTP_CREATED
            );
        } catch (Exception $ex) {
            return $this->sendError(
                ResponseCodes::UNEXPECTED_ERROR,
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $ex->getMessage()
            );
        }
    }

    /**
     * @param Request $request
     * @param Manager $fractal
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, Manager $fractal, $id): JsonResponse
    {
        /** @var PromoCode $promoCode */
        $promoCode = PromoCode::find($id);
        if (!$promoCode) {
            return $this->sendBadRequestResponse('Promo code not found', 404);
        }

        $data = $request->all();
        $validator = new PromoCodeRadiusUpdateValidation($data);
        if (!$validator->validate()) {
            return $this->sendFailure(
                $data,
                Response::HTTP_BAD_REQUEST,
                $validator->getErrors()
            );
        }

        $promoCode->radius = $request->input('radius');
        $promoCode->save();

        $resource = new Item($promoCode, new PromoCodeTransformer());
        return $this->sendSuccess(
            $fractal->createData($resource)->toArray(),
            Response::HTTP_OK
        );
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function deactivate($id): JsonResponse
    {
        $promoCode = PromoCode::find($id);

        if ($promoCode) {
            $promoCode->status = 'inactive';
            return $this->sendSuccess([], Response::HTTP_NO_CONTENT);
        }

        return $this->sendFailure(
            [],
            Response::HTTP_NOT_FOUND,
            'Promo code not found'
        );
    }

    /**
     * @param Manager $fractal
     * @param Request $request
     * @return JsonResponse
     */
    public function checkValidity(Manager $fractal, Request $request): JsonResponse
    {
        $inputs = $request->all();

        $validator = new PromoCodeValidityValidation($inputs);
        if (!$validator->validate()) {
            return $this->sendFailure($inputs, Response::HTTP_BAD_REQUEST, $validator->getErrors());
        }

        /** @var PromoCode $promoCode */
        $promoCode = PromoCode::where('code', $request->input('code'))->with('event')->first();
        if ($promoCode->isInvalid($inputs)) {
            return $this->sendFailure($inputs, Response::HTTP_BAD_REQUEST, 'Invalid promo code');
        }

        $polyline = Polyline::encode([$inputs['origin'], $inputs['destination']]);

        $resource = new Item($promoCode, new PromoCodeTransformer());
        $response = $fractal->createData($resource)->toArray();
        $response['data']['polyline'] = $polyline;

        return $this->sendSuccess($response);
    }
}
