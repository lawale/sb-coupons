<?php

namespace App\Validations;

use Illuminate\Support\Facades\Validator;

abstract class BaseValidation
{
    protected $data;
    protected $messages = [
        'required' => ':attribute is required.',
        'exists' => 'Invalid :attribute specified'
    ];
    protected $rules;

    protected $validator;

    public function __construct($data)
    {
        $this->data = $data;
    }

    protected function getRules(): array
    {
        return $this->rules;
    }

    protected function getValidationMessages(): array
    {
        return $this->messages;
    }

    public function validate(): bool
    {
        $this->validator = Validator::make($this->data, $this->getRules(), $this->getValidationMessages());
        return $this->validator->passes();
    }

    public function getErrors(): string
    {
        $errors = $this->validator->errors()->all();
        return implode("", array_values($errors));
    }
}
