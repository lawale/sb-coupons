<?php

namespace App\Validations;

class PromoCodeCreationValidation extends BaseValidation
{
    protected function getRules(): array
    {
        return [
            'event_id' => ['required', 'integer', 'exists:events,id'],
            'worth' => ['required', 'numeric', 'min:1'],
            'radius' => ['required', 'numeric', 'min:1'],
            'currency' => ['required', 'max:3'],
            'expires_at' => ['required', 'date']
        ];
    }
}
