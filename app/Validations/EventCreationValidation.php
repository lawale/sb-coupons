<?php

namespace App\Validations;

use Illuminate\Support\Facades\Validator;

class EventCreationValidation
{
    private $data;
    private $messages;
    private $rules;

    private $validator;

    public function __construct($data)
    {
        $this->data = $data;

        $this->rules = [
            'name' => ['required', 'max:200', 'unique:events'],
            'longitude' => ['required', 'numeric', 'between:-90,90'],
            'latitude' => ['required', 'numeric', 'between:-90,90'],
            'starts_at' => ['required', 'date', 'before:ends_at'],
            'ends_at' => ['required', 'date'],
        ];

        $this->messages = [
            'required' => ':attribute is required.',
        ];

        $this->validator = Validator::make($this->data, $this->rules, $this->messages);
    }

    public function validate(): bool
    {
        return $this->validator->fails() == true;
    }

    public function getErrorMessage(): string
    {
        $errors = $this->validator->errors()->all();
        return implode("", array_values($errors));
    }

}
