<?php

namespace App\Validations;

class PromoCodeValidityValidation extends BaseValidation
{
    protected function getRules(): array
    {
        return [
            'code' => ['required', 'exists:promo_codes,code'],
            'origin.longitude' => ['required', 'numeric', 'between:-90,90'],
            'origin.latitude' => ['required', 'numeric', 'between:-90,90'],
            'destination.longitude' => ['required', 'numeric', 'between:-90,90'],
            'destination.latitude' => ['required', 'numeric', 'between:-90,90'],
        ];
    }
}
