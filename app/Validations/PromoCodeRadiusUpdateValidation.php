<?php

namespace App\Validations;

class PromoCodeRadiusUpdateValidation extends BaseValidation
{
    protected function getRules(): array
    {
        return [
            'radius' => ['required', 'numeric', 'min:1'],
        ];
    }
}
