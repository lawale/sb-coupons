<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

/**
 * @property mixed $id
 * @property string $code
 * @property string $currency
 * @property numeric $worth
 * @property mixed $updated_at
 * @property mixed $created_at
 * @property mixed $expires_at
 * @property string $status
 * @property numeric $radius
 * @property int $event_id
 *
 * @property Event $event
 */
class PromoCode extends BaseModel
{
    const ACTIVE = 'active';
    const EARTH_RADIUS = 6371000;

    /**
     * Fields that can be updated via update()
     */
    protected $fillable = ['currency', 'worth', 'radius', 'expires_at', 'event_id'];

    /**
     * @param int $length
     * @return string
     */
    public function generateCode(int $length = 8): string
    {
        $code = mb_strtoupper(Str::random($length));

        if ($this->codeExists($code)) {
            $code = $this->generateCode($length);
        }

        return $code;
    }

    /**
     * @param $code
     * @return mixed
     */
    private function codeExists($code)
    {
        return Promocode::where('code', $code)->first();
    }

    /**
     * @return BelongsTo
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::ACTIVE;
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        return ($this->expires_at < Carbon::now()->toDateString());
    }

    /**
     * Credits: https://gist.github.com/Zemistr/787dc929d2b8731465dc
     * @param $latitudeFrom
     * @param $longitudeFrom
     * @param $latitudeTo
     * @param $longitudeTo
     * @return float|int
     */
    public function vincentyGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);

        // distance / 1000 = distance in km
        return ($angle * self::EARTH_RADIUS) / 1000;
    }

    /**
     * @param $origin
     * @param $destination
     * @return bool
     */
    public function withinEventRadius($origin, $destination): bool
    {
        $event = $this->event;

        $distanceFromOrigin = self::vincentyGreatCircleDistance(
            $event->latitude,
            $event->longitude,
            $origin['latitude'],
            $origin['longitude']
        );

        $distanceFromDestination = self::vincentyGreatCircleDistance(
            $event->latitude,
            $event->longitude,
            $destination['latitude'],
            $destination['longitude']
        );
        $radius = floatval($this->radius);

        return $radius >= $distanceFromDestination || $radius >= $distanceFromOrigin;
    }

    /**
     * @param $inputs
     * @return bool
     */
    public function isInvalid($inputs): bool
    {
        return $this->isExpired() || !$this->isActive() ||
            !$this->withinEventRadius($inputs['origin'], $inputs['destination']);
    }
}
