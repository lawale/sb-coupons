<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property mixed $id
 * @property string $name
 * @property double $longitude
 * @property double $latitude
 * @property string $starts_at
 * @property mixed $ends_at
 * @property mixed $updated_at
 * @property mixed $created_at
 *
 * @property PromoCode[] $promoCodes
 */
class Event extends BaseModel
{
    protected $fillable = ['name', 'longitude', 'latitude', 'starts_at', 'ends_at'];

    public function promoCodes(): HasMany
    {
        return $this->hasMany(PromoCode::class, 'event_id', 'id');
    }
}
