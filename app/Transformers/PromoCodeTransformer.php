<?php

namespace App\Transformers;

use App\Models\PromoCode;
use League\Fractal\TransformerAbstract;

class PromoCodeTransformer extends TransformerAbstract
{
    private $includeStatus;

    public function __construct($excludeStatus = true)
    {
        $this->includeStatus = $excludeStatus;
    }

    public function transform(PromoCode $promoCode): array
    {
        $data = [
            'id' => (int)$promoCode->id,
            'code' => $promoCode->code,
            'radius' => $promoCode->radius,
            'worth' => $promoCode->worth,
            'currency' => $promoCode->currency,
            'event_id' => $promoCode->event_id,
            'expires_at' => date('Y-m-d H:i', strtotime($promoCode->expires_at)),
            'created_at' => date('Y-m-d H:i', strtotime($promoCode->created_at)),
            'updated_at' => date('Y-m-d H:i', strtotime($promoCode->updated_at)),
        ];

        if (!$this->includeStatus) {
            $data['status'] = $promoCode->status;
        }

        return $data;
    }
}
