<?php

namespace App\Transformers;

use App\Models\Event;
use League\Fractal\TransformerAbstract;

class EventTransformer extends TransformerAbstract
{
    public function transform(Event $event): array
    {
        return [
            'id' => (int)$event->id,
            'name' => $event->name,
            'longitude' => $event->longitude,
            'latitude' => $event->latitude,
            'starts_at' => date('Y-m-d H:i', strtotime($event->starts_at)),
            'ends_at' => date('Y-m-d H:i', strtotime($event->ends_at)),
            'created_at' => date('Y-m-d H:i', strtotime($event->created_at)),
            'updated_at' => date('Y-m-d H:i', strtotime($event->updated_at)),
        ];
    }
}
