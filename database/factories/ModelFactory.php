<?php

use App\Models\Event;
use App\Models\PromoCode;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;


/** @var Factory $factory */
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});

$factory->define(Event::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'longitude' => 7.4102,
        'latitude' => 3.9165,
        'starts_at' => date('Y-m-d', strtotime("+2 days")),
        'ends_at' => date('Y-m-d', strtotime("+3 days"))
    ];
});

$factory->define(PromoCode::class, function (Faker $faker) {
    return [
        'event_id' => 1,
        'code' => (new PromoCode)->generateCode(),
        'currency' => $faker->currencyCode,
        'radius' => $faker->numberBetween(100, 5000),
        'worth' => $faker->numberBetween(100, 500),
        'status' => 'active',
        'expires_at' => date('Y-m-d', strtotime("+3 days"))
    ];
});
