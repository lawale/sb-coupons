<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var Laravel\Lumen\Routing\Router $router */

$router->get('/', function () use ($router) {
    return response()->json(['status' => 'success', 'current_time' => time()]);
});

$router->group(['prefix' => 'api/v1', 'middleware' => ['json.response']], function ($app) {
    $app->get('events', 'EventsController@index');
    $app->post('events', 'EventsController@create');
    $app->get('events/{id}', 'EventsController@show');
    $app->get('events/{id}/promo_codes', 'EventsController@promo_codes');

    $app->get('promo_codes', 'PromoCodesController@index');
    $app->post('promo_codes', 'PromoCodesController@create');
    $app->patch('promo_codes/{id}/radius', 'PromoCodesController@update');
    $app->delete('promo_codes/{id}', 'PromoCodesController@deactivate');

    $app->post('promo_codes/validate', 'PromoCodesController@checkValidity');
});
