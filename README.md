# Safeboda Promo Code API

## Requirements

The minimum requirement by this project template that your Web server supports PHP 7.2

* [Composer](https://getcomposer.org/doc/00-intro.md#using-composer)

## Install Composer dependencies

```shell 
composer install
```

## Database

This project uses SQLite for data persistence.

1. To create the database file, go to the project root directory and run the following code in the terminal

```shell
touch database/database.sqlite
```

2. To create the tables, run the following command in the terminal

```shell
php artisan migrate
```

## Serving

To launch the PHP inbuilt server,

```shell
 php -S localhost:8000 -t public
```

Go to http://localhost:8000

# Endpoints

## Create Event

This endpoint creates an event

### HTTP Request

`POST /api/v1/events`

> Sample Request

```json
{
    "name": "SB Weekend 2021",
    "longitude": 89.5,
    "latitude": 90,
    "starts_at": "2020-01-20 00:30",
    "ends_at": "2021-01-20 09:00"
}
```

> Sample Response: Success

```json
{
    "status": "success",
    "data": {
        "id": 1,
        "name": "SB Weekend 2021",
        "longitude": 89.5,
        "latitude": 90,
        "starts_at": "2020-01-20 00:30",
        "ends_at": "2021-01-20 09:00",
        "created_at": "2022-01-18 13:13",
        "updated_at": "2022-01-18 13:13"
    }
}
```

> Sample Response: Failure

```json
{
    "status": "fail",
    "message": "The name has already been taken.",
    "data": {
        "name": "SB Weekend 2021",
        "latitude": 7.4092,
        "longitude": 3.9165,
        "starts_at": "2020-01-20 00:30",
        "ends_at": "2021-01-20 09:00"
    }
}
```

### POST Parameters

Parameter | Description | Optional |
--------- | ----------- | ---------|
name | Event name | false |
longitude | Location longitude | false |
latitude | Location latitude | false |
starts_at | Start date and time | false |
ends_at | End date and time | false |

## Get Events

### HTTP Request

`GET /api/v1/events`

> Sample response

```json
{
    "status": "success",
    "data": [
        {
            "id": 1,
            "name": "SB Weekend 2021",
            "longitude": "89.5",
            "latitude": "90",
            "starts_at": "2020-01-20 00:30",
            "ends_at": "2021-01-20 09:00",
            "created_at": "2022-01-18 13:13",
            "updated_at": "2022-01-18 13:13"
        }
    ],
    "meta": {
        "pagination": {
            "total": 1,
            "count": 1,
            "per_page": 15,
            "current_page": 1,
            "total_pages": 1,
            "links": {}
        }
    }
}
```

## Get One Event

### HTTP Request

`GET /api/v1/events/:event_id`

### URL Parameters

Parameter | Description | Optional | 
--------- | ----------- | ---------|
event_id | Event ID | false |

> Sample Response

```json
{
    "status": "success",
    "data": {
        "id": 1,
        "name": "SB Weekend 2022",
        "longitude": "89.5",
        "latitude": "90",
        "starts_at": "2020-01-20 00:30",
        "ends_at": "2021-01-20 09:00",
        "created_at": "2022-01-17 15:57",
        "updated_at": "2022-01-17 15:57"
    }
}
```

## Create Promo Code

This endpoint creates a promo code for an event

### HTTP Request

`POST /api/v1/promo_codes`

> Sample Request

```json
{
    "event_id": 1,
    "worth": 100,
    "currency": "NGN",
    "radius": 200,
    "expires_at": "2022-09-30 08:40"
}
```

> Sample Response: Success

```json
{
    "status": "success",
    "data": {
        "id": 7,
        "code": "AEEAAI75",
        "radius": 200,
        "currency": "NGN",
        "status": "active",
        "event_id": 1,
        "expires_at": "2022-09-30 08:40",
        "created_at": "2022-01-18 14:51",
        "updated_at": "2022-01-18 14:51"
    }
}
```

> Sample Response: Failure

```json
{
    "status": "fail",
    "message": "radius is required.",
    "data": {
        "event_id": 1,
        "worth": 100,
        "currency": "NGN",
        "expires_at": "2022-09-30 08:40"
    }
}
```

### POST Parameters

Parameter | Description | Optional |
--------- | ----------- | ---------|
event_id | Entity id of the event linked to promo code | false |
worth | Promo code worth, in national currency | false |
currency | ISO currency code | false |
radius | Radius of validity around event location | false |
expires_at | Promo code expiration date and time | false |

## Retrieve Promo Codes

### HTTP Request

`GET /api/v1/promo_codes`

### Query Parameters

Parameter | Description | Optional | 
--------- | ----------- | ---------|
is_active | Flags to return either only active or all codes   | true |

> Sample Response: Return ALL

```json
{
    "status": "success",
    "data": [
        {
            "id": 2,
            "code": "VEA8L337",
            "radius": "200",
            "currency": "NGN",
            "status": "inactive",
            "event_id": "1",
            "expires_at": "2022-09-30 08:40",
            "created_at": "2022-01-18 14:51",
            "updated_at": "2022-01-18 14:51"
        },
        {
            "id": 3,
            "code": "AWPPJQBY",
            "radius": "200",
            "currency": "NGN",
            "status": "active",
            "event_id": "1",
            "expires_at": "2022-09-30 08:40",
            "created_at": "2022-01-18 14:51",
            "updated_at": "2022-01-18 14:51"
        },
        {
            "id": 4,
            "code": "G9IK9AFW",
            "radius": "200",
            "currency": "NGN",
            "status": "active",
            "event_id": "1",
            "expires_at": "2022-09-30 08:40",
            "created_at": "2022-01-18 14:51",
            "updated_at": "2022-01-18 14:51"
        }
    ],
    "meta": {
        "pagination": {
            "total": 3,
            "count": 3,
            "per_page": 15,
            "current_page": 1,
            "total_pages": 1,
            "links": {}
        }
    }
}
```

> Sample Response: Return ONLY Active

```json
{
    "status": "success",
    "data": [
        {
            "id": 2,
            "code": "VEA8L337",
            "radius": "200",
            "currency": "NGN",
            "event_id": "1",
            "expires_at": "2022-09-30 08:40",
            "created_at": "2022-01-18 14:51",
            "updated_at": "2022-01-18 14:51"
        },
        {
            "id": 3,
            "code": "AWPPJQBY",
            "radius": "200",
            "currency": "NGN",
            "event_id": "1",
            "expires_at": "2022-09-30 08:40",
            "created_at": "2022-01-18 14:51",
            "updated_at": "2022-01-18 14:51"
        },
        {
            "id": 4,
            "code": "G9IK9AFW",
            "radius": "200",
            "currency": "NGN",
            "event_id": "1",
            "expires_at": "2022-09-30 08:40",
            "created_at": "2022-01-18 14:51",
            "updated_at": "2022-01-18 14:51"
        }
    ],
    "meta": {
        "pagination": {
            "total": 3,
            "count": 3,
            "per_page": 15,
            "current_page": 1,
            "total_pages": 1,
            "links": {}
        }
    }
}
```

## Configure Promo Code Radius

This endpoint update the radius for a promo code

### HTTP Request

`PATCH /api/v1/promo_codes/:id/radius`

### URL Parameters

Parameter | Description | Optional | 
--------- | ----------- | ---------|
id | Promo Code ID | false |

### POST Parameters

Parameter | Description | Optional | 
--------- | ----------- | ---------|
radius | Radius of validity around event location | false |

> Sample Request

```json
{
    "radius": 200
}
```

> Sample Response: Success

```json
{
    "status": "success",
    "data": {
        "id": 7,
        "code": "AEEAAI75",
        "radius": 200,
        "currency": "NGN",
        "status": "active",
        "event_id": 1,
        "expires_at": "2022-09-30 08:40",
        "created_at": "2022-01-18 14:51",
        "updated_at": "2022-01-18 14:51"
    }
}
```

> Sample Response: Failure

```json
{
    "status": "fail",
    "message": "Promo code not found",
    "data": []
}
```

### POST Parameters

Parameter | Description | Optional |
--------- | ----------- | ---------|
event_id | Entity id of the event linked to promo code | false |
worth | Promo code worth, in national currency | false |
currency | ISO currency code | false |
radius | Radius of validatiy around event location | false |
expires_at | Promo code expiration date and time | false |

## Deactivate Promo Code

This endpoint deactivates promo code

### HTTP Request

`DELETE /api/v1/promo_codes/:id`

### URL Parameters

Parameter | Description | Optional | 
--------- | ----------- | ---------|
id | Promo Code ID | false |


> Sample Response: Success

```json
{

}
```

> Sample Response: Failure

```json
{
    "status": "fail",
    "message": "Promo code not found",
    "data": []
}
```

### POST Parameters

Parameter | Description | Optional |
--------- | ----------- | ---------|
event_id | Entity id of the event linked to promo code | false |
worth | Promo code worth, in national currency | false |
currency | ISO currency code | false |
radius | Radius of validatiy around event location | false |
expires_at | Promo code expiration date and time | false |

## Check Promo Code Validity

This endpoint update the radius for a promo code

### HTTP Request

`POST /api/v1/promo_codes/validate`


> Sample Request

```json
{
    "code": "ECUCQL52",
    "origin": {
        "latitude": 7.4092,
        "longitude": 3.9165
    },
    "destination": {
        "latitude": 7.4192,
        "longitude": 3.9165
    }
}
```

> Sample Response: Success

```json
{
    "status": "success",
    "data": {
        "id": 7,
        "code": "AEEAAI75",
        "radius": 200,
        "currency": "NGN",
        "status": "active",
        "event_id": 1,
        "expires_at": "2022-09-30 08:40",
        "created_at": "2022-01-18 14:51",
        "updated_at": "2022-01-18 14:51",
        "polyline": "kjhkh"
    }
}
```

> Sample Response: Failure

```json
{
    "status": "fail",
    "message": "Promo code not found",
    "data": []
}
```

### POST Parameters

Parameter | Description | Optional | 
--------- | ----------- | ---------|
code | Radius of validity around event location | false |
origin.latitude | Latitude of ride origin | false |
origin.longitude | Latitude of ride origin | false |
destination.latitude | Latitude of ride destination | false |
destination.longitude | Latitude of ride destination | false |
